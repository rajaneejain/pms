class LoginsController < ApplicationController
  def index
  	 if user_signed_in?
  		redirect_to accounts_path
  	 else
  	     render :index
  	 end    			
  end
end
