Rails.application.routes.draw do
  devise_for :users
  get 'logins/index'
 
  root 'logins#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  # resources :projects
  # resources :tasks
  resources :accounts do
    member do
      get :withdraw
      post :withdraw, to: "accounts#create_withdraw"
      get :transfer
      post :transfer, to: "accounts#create_transfer"
    end
    collection do
      get :deposit
      post :deposit, to: "accounts#create_deposit"
    end  
  end
end
